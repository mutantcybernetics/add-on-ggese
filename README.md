# Add-on-GGESE

# GPS + WiFi + Bluetooth  + Micro SD + Gyro

## Compatible with mutantC v2/v3/v4(with modification)

Can able to turn off each module separately using Pi GPIO.

It has this module support (Click the blue color name to show the module)
- [Ubox-GPS](https://www.ebay.com/sch/i.html?_nkw=gyneo6mv2) (only module, no PCB)
- [ESP-32](https://www.ebay.com/sch/i.html?_nkw=ESP-WROOM-32+module)
- [ESP-8266](https://www.ebay.com/sch/i.html?_nkw=ESP8266+Serial+WIFI+Wireless+Transceiver+Module)
- [Micro-SD](https://www.ebay.com/sch/i.html?_nkw=Mini_SD_Memory_Module_for_Arduino_SPI_Interface) (connected with ESP-32)
- [Gyro-MPU6050](https://www.ebay.com/sch/i.html?_nkw=mpu6050)


<img src="pic_main.png" width="500">
<img src="position.png" width="500">
<img src="show_off.png" width="500">
